require 'rails_helper'

RSpec.describe NavigatorController, type: :controller do
    
    context "when there is no project" do
        describe "GET #navigator" do
            it "should returns success" do
               get :navigator
               expect(response).to have_http_status(:success)
            end
        end
    end
    
    context "when there is at least one project" do
        describe "GET #navigator" do
            
        end
    end

end
