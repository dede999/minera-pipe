require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do

  describe "GET #show" do
    it "returns http success" do
      p = Project.create(title: "Teste", process: "14/16484-4", start_date: "2016-01-01", end_date: "2017-01-01")
      get :show, id: p.id
      expect(response).to have_http_status(:success)
    end

    it "returns http error" do
      get :show, id: -1
      expect(response).to have_http_status(:not_found)
    end
  end

  describe "GET #table" do
    it "returns http success" do
      get :table
      expect(response).to have_http_status(:success)
			    
		end
  end

  describe "GET #cluster" do
  	before do
  		@project1 = Project.create(:process => '14/50774-5', :title => 'test1', :end_date => '2016/10/11', :start_date => '2012/10/2')
		  @project2 = Project.create(:process => '14/50774-5', :title => 'test2', :end_date => '2016/10/11', :start_date => '2012/10/2')
  		NavigatorController.cmembers = [[@project1.id, @project2.id], [], [], []]
  	end
		context 'when receives params' do
			it "returns http success when all params[id] match" do
   	    get :cluster, id: 0
   	    expect(assigns(:projects).count).to eq(2)
        expect(response).to have_http_status(:success) 
			end
 			it "returns http not found when all params[id] dont match" do
				get :cluster, id: -1
	    	expect(assigns(:projects)).to be_nil 
	    	expect(response).to have_http_status(:not_found)
			end
			it "returns http not found when params[id] are empty" do
		    get :cluster, id: nil
		    expect(assigns(:projects)).to be_nil 
		    expect(response).to have_http_status(:not_found)
			end  
		end

		context 'when not receives params' do
			it "returns http not found when there is no params[id]" do
		   	get :cluster
		    expect(response).to have_http_status(:not_found) 
			end
		end
  end
end
