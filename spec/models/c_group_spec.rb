require 'rails_helper'

RSpec.describe CGroup, type: :model do
	describe 'should work' do
		it 'working example' do
			test = CGroup.new(:clusters_ids => '5 6 7 8 9', :size => 5)
			expect(test.save!).to be true
		end
	end

	describe 'should break' do
		it 'no ids' do
			test = CGroup.new(:size => 5)
			expect(test.save).to be false
		end

		it 'no words' do
			test = CGroup.new(:clusters_ids => '5 6 7 8 9')
			expect(test.save).to be false
		end
	end

	describe 'CGroup method' do
		it 'clusterIdsArray' do
			test = CGroup.new(:clusters_ids => '5 6 7 8 9', :size => 5)
			test.save!
			expect(test.clusterIdsArray).to match_array([5, 6, 7, 8, 9])
		end
	end
end
