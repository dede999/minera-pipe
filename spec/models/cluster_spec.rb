require 'rails_helper'

RSpec.describe Cluster, type: :model do
	describe 'should work' do
		it 'working example' do
			test = Cluster.new(:ids => '127 45 32', :palavras => 'exemplo de palavras', :frequencia => '8 4 12')
			expect(test.save!).to be true
		end
	end

	describe 'should break' do
		it 'no ids' do
			test = Cluster.new(:palavras => 'exemplo de palavras', :frequencia => '8 4 12')
			expect(test.save).to be false
		end

		it 'no words' do
			test = Cluster.new(:ids => '127 45 32', :frequencia => '8 4 12')
			expect(test.save).to be false
		end

		it 'no frequencies' do
			test = Cluster.new(:ids => '127 45 32', :palavras => 'exemplo de palavras')
			expect(test.save).to be false
		end
	end

	describe 'Cluster methods' do
		it 'projectIdsArray' do
			test = Cluster.new(:ids => '127 45 32', :palavras => 'exemplo de palavras', :frequencia => '8 4 12')
			test.save!
			expect(test.projectIdsArray).to match_array([127, 45, 32])
		end

		it 'tags' do
			test = Cluster.new(:ids => '127 45 32', :palavras => 'exemplo de palavras', :frequencia => '8 4 12')
			test.save!
			expect(test.tags).to include({:text=>"exemplo", :weight=>"8"}, {:text=>"de", :weight=>"4"}, {:text=>"palavras", :weight=>"12"})
		end
	end
end
