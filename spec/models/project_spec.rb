require 'rails_helper'

RSpec.describe Project, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  describe 'deve funcionar' do
    it 'exemplo que funciona bem' do
      teste = Project.new(:process => '16/00774-4', :title =>  'Aprimoramento do sistema CleverCare', :start_date => '2016-10-01', :end_date => '2017-06-30', :subject => 'Brasil')
      expect(teste.save!).to be true
    end
  end

  describe 'deve quebrar' do
    it 'processo em formato equivocados' do
      teste = Project.new(:process => '16//888', :title => 'Aprimoramento', :start_date => '2016-10-01', :end_date => '2017-06-30')
      expect(teste.save).to be false
    end

    it 'titulo pequeno' do
      teste = Project.new(:process => '16/00774-4', :title =>  'LOL', :start_date => '2016-10-01', :end_date => '2017-06-30')
      expect(teste.save).to be false
    end

    it 'equivoco em datas' do
      teste = Project.new(:process => '16/00774-4', :title =>  'Aprimoramento do sistema CleverCare', :start_date => '2018-10-01', :end_date => '2017-06-30')
      expect{teste.save!}.to raise_error
    end
  end

  describe 'test subject attribute' do
    it 'project should have subject' do
      test = Project.new(:process => '16/00774-4', :title =>  'Aprimoramento do sistema CleverCare', :start_date => '2016-10-01', :end_date => '2017-06-30', :subject => 'Brasil:Argentina:Eduardo:Davi')
      expect(test.subjects).to include 'Brasil'
      expect(test.subjects).to include 'Argentina'
      expect(test.subjects).to include 'Eduardo'
      expect(test.subjects).to include 'Davi' 
    end
  end
end

# numero de processo -- \d{2}\/\d+\-\d
# título --  pelo menos 5 chars \w{5}\w*
#datta fim >data inicio


#20/256211-4 - process id
#título
