Rails.application.routes.draw do
  get 'projects/show'

  get 'tag_cloud/index'
  
  get 'navigator/navigator'

  get 'projects/table'

	get 'projects/cluster', to: 'projects#cluster'

  get '/projects/:id', to: 'projects#show'
  
  root 'navigator#navigator'

  get  '/projects/:id/en', to: 'projects#show_en'

  get 'searchid/searchid'
end
