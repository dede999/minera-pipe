# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161207123725) do

  create_table "c_groups", force: :cascade do |t|
    t.string   "clusters_ids"
    t.integer  "size"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "clusters", force: :cascade do |t|
    t.string   "ids"
    t.string   "palavras"
    t.string   "frequencia"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "project_research_area_relations", force: :cascade do |t|
    t.integer "project_id"
    t.integer "research_area_id"
  end

  add_index "project_research_area_relations", ["project_id"], name: "index_project_research_area_relations_on_project_id"
  add_index "project_research_area_relations", ["research_area_id"], name: "index_project_research_area_relations_on_research_area_id"

  create_table "project_researcher_relations", force: :cascade do |t|
    t.integer "project_id"
    t.integer "researcher_id"
  end

  add_index "project_researcher_relations", ["project_id"], name: "index_project_researcher_relations_on_project_id"
  add_index "project_researcher_relations", ["researcher_id"], name: "index_project_researcher_relations_on_researcher_id"

  create_table "projects", force: :cascade do |t|
    t.string   "process"
    t.string   "title"
    t.string   "title_en"
    t.string   "institution"
    t.string   "institution_city"
    t.string   "partner_institution"
    t.string   "enterprise"
    t.string   "city"
    t.string   "search_local"
    t.string   "funding_line"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "cooperation_FAPESP"
    t.string   "foreign_institution"
    t.string   "abstract"
    t.string   "abstract_en"
    t.string   "related_processes"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "responsible_researcher_id"
    t.integer  "superviser_id"
    t.integer  "visiting_researcher_id"
    t.integer  "foreign_responsable_researcher_id"
    t.string   "subject"
  end

  add_index "projects", ["foreign_responsable_researcher_id"], name: "index_projects_on_foreign_responsable_researcher_id"
  add_index "projects", ["responsible_researcher_id"], name: "index_projects_on_responsible_researcher_id"
  add_index "projects", ["superviser_id"], name: "index_projects_on_superviser_id"
  add_index "projects", ["visiting_researcher_id"], name: "index_projects_on_visiting_researcher_id"

  create_table "research_areas", force: :cascade do |t|
    t.string   "name"
    t.string   "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "researchers", force: :cascade do |t|
    t.string   "name"
    t.string   "institution", default: ""
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

end
