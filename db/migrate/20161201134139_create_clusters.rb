class CreateClusters < ActiveRecord::Migration
  def change
    create_table :clusters do |t|
      t.string :ids
      t.string :palavras
      t.string :frequencia
      t.timestamps null: false
    end
  end
end
