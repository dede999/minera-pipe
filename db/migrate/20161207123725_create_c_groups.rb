class CreateCGroups < ActiveRecord::Migration
  def change
    create_table :c_groups do |t|

      t.string :clusters_ids
      t.integer :size
      t.timestamps null: false
    end
  end
end
