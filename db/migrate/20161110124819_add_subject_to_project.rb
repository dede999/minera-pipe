class AddSubjectToProject < ActiveRecord::Migration
  def change
    add_column :projects, :subject, :string
  end
end
