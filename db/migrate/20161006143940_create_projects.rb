class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
			t.string :process
			t.string :title
			t.string :title_en
			t.string :institution
			t.string :institution_city
			t.string :partner_institution
			t.string :enterprise
			t.string :city
			t.string :search_local
			t.string :funding_line
			t.timestamp :start_date
			t.timestamp :end_date
			t.string :cooperation_FAPESP
			t.string :foreign_institution
			t.string :abstract
			t.string :abstract_en
			t.string :related_processes
      t.timestamps null: false
    end
  end
end
