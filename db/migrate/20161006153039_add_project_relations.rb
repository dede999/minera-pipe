class AddProjectRelations < ActiveRecord::Migration
  def change
		add_reference :projects, :responsible_researcher, references: :researchers, index: true
		add_reference :projects, :superviser, references: :researchers, index: true
		add_reference :projects, :visiting_researcher, references: :researchers, index: true
		add_reference :projects, :foreign_responsable_researcher, references: :researchers, index: true
  end
end
