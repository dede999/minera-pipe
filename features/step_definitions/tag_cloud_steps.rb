require 'uri'
require 'cgi'
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

module WithinHelpers
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
end
World(WithinHelpers)

When /^I open the page$/ do
  visit path_to("the home page")
end

Then /^The title should be "(.+)"$/ do |title|
  assert page.has_content?(title)
end

When(/^I go to the project table page$/) do
	visit path_to("projects table page")
end

Then(/^I should see a table "([^"]*)"$/) do |arg1|
  page.should have_css("table#{arg1}")
end

Then(/^I should see a column "([^"]*)"$/) do |arg1|
  page.should have_css("th#{arg1}")
end
