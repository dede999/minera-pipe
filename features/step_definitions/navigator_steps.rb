require 'uri'
require 'cgi'
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

module WithinHelpers
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
end
World(WithinHelpers)

When(/^I am on the home page$/) do
  visit path_to("the navigator page")
end

When(/^I change the number of cluster to (\d+)$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^I should see a cluster with id "([^"]*)"$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end
