require 'uri'
require 'cgi'
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

module WithinHelpers
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
end
World(WithinHelpers)

Given(/^A project with title "([^"]*)" exists$/) do |arg1|
  Project.create(title: arg1, process: "14/16484-4", start_date: "2016-01-01", end_date: "2017-01-01")
end

When(/^I go to a project "([^"]*)" page$/) do |arg1|
  project = Project.find_by_title(arg1)
  visit view_proj project.id
end

Then(/^Page "([^"]*)" has abstract$/) do |arg1|
  project = Project.find_by_title(arg1)
  expect(page.has_content?(project.abstract)).to be true
end

Given(/^I try to reach an Invalid project$/) do
  visit path_to("the show page for -4")
end

Then(/^I should go to Page Not Found$/) do
  expect(page.has_content?("The page you were looking for doesn't exist.")).to be true
end
