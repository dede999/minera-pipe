require 'uri'
require 'cgi'
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

module WithinHelpers
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
end
World(WithinHelpers)


When(/^I go to the home page$/) do
  visit path_to("the home page")
end

Then(/^I should see a top menu$/) do
  page.should have_css(".cabecalho")
end
