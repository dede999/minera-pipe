Feature: Project

  Scenario: View Project
    Given A project with title "Project 1" exists
    When I go to a project "Project 1" page
    Then Page "Project 1" has abstract

  Scenario: View Failure
    Given I try to reach an Invalid project
    Then I should go to Page Not Found