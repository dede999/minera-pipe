Feature: Projects table

Scenario: User sees projects table
When I go to the project table page
Then I should see a table ".project-table"

Scenario: User sees projects table columns
When I go to the project table page
Then I should see a column ".title"
And I should see a column ".enterprise"
And I should see a column ".research-area"
And I should see a column ".start-date"
And I should see a column ".end-date"
And I should see a column ".more-details"

