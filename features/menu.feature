Feature: Menu

	Scenario: View Menu on the home page
		When I go to the home page
		Then I should see a top menu

	Scenario: View Menu on the project table page
		When I go to the project table page
		Then I should see a top menu