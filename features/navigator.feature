Feature: Navigator

  Scenario: Define number of clusters
    When I am on the home page
    When I change the number of cluster to 7
    Then I should see a cluster with id "tag_6"

  Scenario: The number of cluster are kept when go to the next page

  Scenario: Number of projects is reasonable

  Scenario: Changing the number of clusters does not affect the current projects

  Scenario: Max number of clusters is 10

  Scenario: Min number of cluster is 2
