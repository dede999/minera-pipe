require 'cluster/headlines'

for k in 2..10
  c = Clusterizer.new(k)
  groups, cmembers = c.clusters("all")
  cg = CGroup.new
  String group_id = ""
  groups.each_with_index do |tag, index|
    cluster = Cluster.new
    ids = ""
    palavras = ""
    frequencias = ""
    tag.each do |key, value|
      palavras << key.to_s
      palavras << " "
      frequencias << value.to_s
      frequencias << " "
    end
    cmembers[index].each do |id|
      ids << id.to_s
      ids << " "
    end

    cluster.ids = ids
    cluster.palavras = palavras
    cluster.frequencia = frequencias
    cluster.save!
    group_id << cluster.id.to_s << " "
  end
  cg.size = k
  cg.clusters_ids = group_id
  cg.save!
end