require 'csv'

CSV.foreach(File.join(File.dirname(__FILE__), 'data_with_subject.csv'), :headers => true, :col_sep => ";") do |row|
	project = Project.new
	project.process = row.first.second #row["N. Processo"]
  project.subject = row["Assuntos"]	
  project.title = row["Título (Português)"]
	project.title_en = row["Título (Inglês)"]
	project.institution = row["Instituição"]
	project.institution_city = row["Cidade Instituição"]
	project.partner_institution = row["Instituição Parceira"]
	project.enterprise = row["Empresa"]
	project.city = row["Município"]
	project.search_local = row["Local de Pesquisa"]
	project.funding_line = row["Linha de Fomento"]
	date = row["Data de Início"].split('-')
	year = date[0]
	month = date[1]
	day = date[2]
	project.start_date = Time.new(year,month,day)
	date = row["Data de Término"].split('-')
	year = date[0]
	month = date[1]
	day = date[2]
	project.end_date = Time.new(year,month,day)
	project.cooperation_FAPESP = row["Acordo(s)/Convênio(s) de Cooperação com a FAPESP"]
	project.foreign_institution = row["Instituições no Exterior"]
	project.abstract = row["Resumo (Português)"]
	project.abstract_en = row["Resumo (Inglês)"]
	project.related_processes = row["Processos Vinculados"]	
	project.save!	


	responsible_researcher = Researcher.find_or_create_by(name: row["Pesquisador Responsável"])
	project.responsible_researcher = responsible_researcher
	superviser = Researcher.find_or_create_by(name: row["Supervisor"])
	project.superviser = superviser
	visiting_researcher = Researcher.find_or_create_by(name: row["Pesquisador Visitante"])
	visiting_researcher.institution = row["Instituição do Pesquisador Visitante"]
	visiting_researcher.save
	project.visiting_researcher = visiting_researcher
	foreign_responsable_researcher = Researcher.find_or_create_by(name: row["Pesquisador responsável no exterior"])
	project.foreign_responsable_researcher = foreign_responsable_researcher

	unless row["Pesquisadores Principais"].blank?
		row["Pesquisadores Principais"].split(",").each do |name|
			researcher = Researcher.find_or_create_by(name: name.lstrip)
			project.researchers << researcher
		end	
	end

	research_area = ResearchArea.find_or_create_by(name: row["Grande Área do Conhecimento"], level: "great")
	project.research_areas << research_area	
	research_area = ResearchArea.find_or_create_by(name: row["Área do Conhecimento"], level: "none")
	project.research_areas << research_area	
	research_area = ResearchArea.find_or_create_by(name: row["Subárea do Conhecimento"], level: "sub")
	project.research_areas << research_area	

	if project.save
		print "."
	else 
		print "f"
	end
end
