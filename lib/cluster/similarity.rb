class Similarity
  require 'rubygems'
  require 'bundler/setup'
  #require_relative 'kmeans-clusterer'
  #require_relative 'bag'
  require 'cluster/kmeans-clusterer'
  require 'cluster/bag'
  require 'optparse'
  require 'rake_text'

  def self.similar project
    docs = []
    doc_ids = []
    wfreq = []
    wnote = []
    cmember = []
    centroid_ids = []
    project_index = nil
    ids = Array.new

    bag = BagOfWords.new idf: true
    filename = "lib/cluster/ignoredWords.txt"
    ignoredWords = File.open(filename).read.split(/\n/)

    view_list = Project.abstracts

    view_list.each do |key, value|
      unless value.nil?
        value.downcase!
        words = value.split(/ |, |,|\.|"|;|:|\(|\)|\/|\[|\]|\{|\}/)
        relev = ""
        words.each do |i|
          if i.size > 3
            if not ignoredWords.include? i
              relev << " #{i}"
            end
          end
        end
        if relev.size > 500
          bag.add_doc relev
          docs << relev
          doc_ids << key
          if project.id == key
            project_index = doc_ids.size - 1
          end
        end
      end
    end

    return nil unless project_index

    data = bag.to_matrix
    centroid_ids << project_index
    row_norms = KMeansClusterer::Scaler.row_norms(data)
    centroids = data[true, centroid_ids]

    distances = KMeansClusterer::Distance.euclidean(centroids, data, row_norms)
    dist_hash = Hash.new
    i = 0
    points_count = data ? data.shape[1] : 0
    while i < points_count
      dist_hash[i] = distances[i, 0]
      i += 1
    end


    sorthash = dist_hash.sort_by { |id, dist| dist }
    sorthash.shift
    dens0 = 0.0
    i = 0
    r_dist = sorthash[i][1]
    dens1 = (i + 1)/r_dist**(points_count*0.15)
    while  (i < 4 or dens1 > dens0*(1.0-0.10*5.0/(5.0+i))) and not i >= 20
      i += 1
      r_dist = sorthash[i][1]
      dens0 = dens1
      dens1 = (i + 1)/r_dist**(points_count*0.15)
      ids << sorthash[i][0]
    end
    ps = Array.new
    ids.each do |x|
      ps << Project.find(x)
    end
    return ps
  end
end
