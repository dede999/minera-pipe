class AreasSample
	def self.Areas
		@@Areas
	end
	@@Areas = [
["Ciências Agrárias", "Agronomia", "Fitossanidade"],
["Ciências Exatas e da Terra", "Engenharia Química", "Tecnologia Química"],
["Engenharias", "Engenharia Química", "Processos Industriais de Engenharia Química"],
["Engenharias", "Engenharia de Transportes", "Operações de Transportes"],
["Engenharias", "Engenharia Mecânica", "Projetos de Máquinas"],
["Ciências Agrárias	Ciência e Tecnologia de Alimentos	Engenharia de Alimentos"],
["Ciências Exatas e da Terra	Ciência da Computação	Metodologia e Técnicas da Computação"]
	]
end

