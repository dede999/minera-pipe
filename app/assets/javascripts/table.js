$(function(){
  $("#myTable").tablesorter();
  $("#myTable").bind("sortEnd", function(){
    $("#myTable tr").each(function(index){
      if(index%2 == 0){
        $( this ).removeClass("impar");
        $( this ).addClass("par");
      } else {
        $( this ).removeClass("par");
        $( this ).addClass("impar");
      }
    })
  })
});