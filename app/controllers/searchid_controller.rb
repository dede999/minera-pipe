class SearchidController < ApplicationController
  require 'cluster/similarity'

  def searchid
    @projects = Similarity.similar (Project.find(params[:search]))
  end
end
