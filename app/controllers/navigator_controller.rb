
class NavigatorController < ApplicationController
    cattr_accessor :cmembers
    require 'cluster/headlines'
    @@cmembers = [[], [], [], []]
    @@tag_array = [{}, {}, {}, {}]
    def self.tag_array
        @@tag_array
    end

    def self.cmembers
        @@cmembers
    end

    def navigator
        @size = 400
        @@tag_array = []
        params[:view_projects] ||= "all"
        params[:n_cluster] ||= 6
        params[:keep_cluster] ||= "false"
        k  = params[:n_cluster].to_i
        temp = @@cmembers.length
        if k < temp
            for i in 1..(temp - k)
                @@cmembers.pop
            end
        end
        begin
            c = Clusterizer.new(params[:n_cluster].to_i)
            if params[:view_projects] == "all"
                clusters = CGroup.where("size = ?", k).first
                if clusters != nil
                    clusters.clusterIdsArray.each_with_index do |id, index|
                        cluster = Cluster.find(id)
                        #puts cluster
                        #cluster.each_with_index do |tag, index| 
                        @@tag_array << cluster.tags
                        @@cmembers[index] = cluster.projectIdsArray
                      #end
                    end
                    groups = []
                else
                  groups, @@cmembers = c.clusters(params[:view_projects])
                end
                #params[:view_projects] = ""
            else
                if params[:keep_cluster] == "true"
                    aux = @@cmembers.flatten
                    groups, @@cmembers = c.clusters (aux)
                else
                    groups, @@cmembers = c.clusters (@@cmembers[params[:view_projects].to_i])
                end
                @@view_paths ||= params[:view_projects].to_i
                #if @@view_paths <= params[:view_projects].to_i
                @@view_paths = params[:view_projects].to_i
                #else
                 #   params[:view_projects] = ""
                 #   @@view_paths = nil
                 #   redirect_to navigator_navigator_path
                #end
            end
            groups.each do |tag|
                array = []
                tag.each do |key, value|
                    array << {:text => key.to_s, :weight => value.to_s}
                end
                @@tag_array << array
            end
            @cmembers = @@cmembers
            @tag_array = @@tag_array
        rescue
            params[:n_cluster].to_i.times do
                @@tag_array << {}
            end
        end
    end
end
