class ProjectsController < ApplicationController

  def project_params
    params.require(:project).permit(:id, :ids[], :tag_array[])
  end

  def show
    begin
      id = params[:id]
      @project = Project.find(id)
    rescue ActiveRecord::RecordNotFound
      render :file => "#{Rails.root}/public/404.html",  :status => 404
    end
  end

  # GET /projects/1/en
  # metodo de exibição em inglês
  def show_en
    begin
      id = params[:id]
      @project = Project.find(id)
    rescue ActiveRecord::RecordNotFound
      render :file => "#{Rails.root}/public/404.html",  :status => 404
    end
  end

  def table
		@projects = Project.all
  end

	def cluster
    @tag_array = NavigatorController.tag_array[params[:id].to_i]
		if params[:id].blank? or params[:id].to_i < 0 or NavigatorController.cmembers[params[:id].to_i].blank? or Project.find_by_id(NavigatorController.cmembers[params[:id].to_i]).blank?
			@projects = nil
			render :status => 404		
		else	
			@projects = Project.where('id in (?)', NavigatorController.cmembers[params[:id].to_i])
		end	
	end

end
