class ProjectResearchAreaRelation < ActiveRecord::Base
	belongs_to :projects
	belongs_to :research_area
end
