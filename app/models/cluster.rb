class Cluster < ActiveRecord::Base
  
  validates :ids,
            :presence => { :message => 'É um dado necessário' }

  validates :palavras,
            :presence => { :message => 'É um dado necessário' }

  validates :frequencia,
            :presence => { :message => 'É um dado necessário' } 

  def projectIdsArray
     begin
       array = []
       idsString = self.ids.split(' ')
       idsString.each do |id|
         array << id.to_i
       end
       array
     rescue NoMethodError
       []
     end
   end

   def tags
    tags = []
    begin
      p = self.palavras.split(' ')
    rescue NoMethodError
      {}
    end

    begin
      f = self.frequencia.split(' ')
    rescue NoMethodError
      {}
    end
    p.each_with_index do |palavra, index|
        tags << {:text => palavra, :weight => f[index]}
    end
    tags
   end
end
