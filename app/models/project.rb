class Project < ActiveRecord::Base
	belongs_to :responsible_researcher, class_name: 'Researcher'
	belongs_to :superviser, class_name: 'Researcher'
	belongs_to :visiting_researcher, class_name: 'Researcher'
	belongs_to :foreign_responsable_researcher, class_name: 'Researcher'

	has_many :project_research_area_relations
	has_many :research_areas, through: :project_research_area_relations

	has_many :project_researcher_relations
	has_many :researchers, through: :project_researcher_relations

	validates :process,
						:allow_blank => false,
						:format => { :with => %r{\d{2}\/\d*\-\d}, :message => 'Este numero de processo não é valido' }

	validates :end_date,
						:presence => { :message => 'É um dado necessário' }


	validates :start_date,
						:presence => { :message => 'É um dado necessário' }
	
    validate :valid_date?

	def valid_date?
        errors.add(:start_date, "deve ser anterior a end date") unless
            start_date < end_date
	end

	validates :title,
						:length => { :minimum => 5, :message => 'O título deve ter, no mínimo, 5 caracteres' }

	def self.abstracts
		hash = {}
		array = Project.pluck('id, abstract, subject')
 		array.each do |pair|
			if pair.third != nil
				subjects = pair.third.gsub("-", " ")
				subjects = subjects.gsub(":", " ")
				if pair.second != nil
					hash[pair.first] = pair.second + (" " + subjects) * 12
				else
					hash[pair.first] = subjects * 6
				end
			else
				hash[pair.first] = pair.second
			end
		end
		hash
	end

  def subjects
    begin
      self.subject.split(':')
    rescue NoMethodError
      []
    end
  end

	def research_area
		sub = research_areas.where(level: 'sub').first
		none = research_areas.where(level: 'none').first
		great = research_areas.where(level: 'great').first
		return sub.name unless sub.blank? or sub.name.blank?
		return none.name unless none.blank? or none.name.blank?
		return great.name
	end
end
