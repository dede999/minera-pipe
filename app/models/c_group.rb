class CGroup < ActiveRecord::Base
  
  validates :clusters_ids,
            :presence => { :message => 'É um dado necessário' }

  validates :size,
            :presence => { :message => 'É um dado necessário' }

  def clusterIdsArray
    begin
      array = []
      ids = self.clusters_ids.split(' ')
      ids.each do |id|
        array << id.to_i
      end
      array
    rescue NoMethodError
      []
    end
  end
end
