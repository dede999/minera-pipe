# Review - 14/12/2016 - Phase 4

* Reviewed by Arthur Del Esposte
* Version: 2484c43a9a54544202f19544535f8b7eabfbafe5

## Verifications

* [~] Test passing
  * [x] RSPEC: 28 examples, 0 failures, 5 pending
  * [ ] Cucumber: 12 scenarios (3 failed, 9 passed)
* [x] Front-end
  * [x] Page HEADER - Top menu
  * [x] Improve Cloud Tag visualization
* [x] Present similar projects
* [x] Save clusters in database
* [x] Code Refactoring
  * [x] Remove puts
  * [x] Use partials

## Comments

* Cluster creation script breaks when there is no project in database
* Cluster creation script stucks while creating 9 clusters
* Commits messages in PT-BR
* Commits with the Signed-off-by as message
* The system does not work when there is no project in database
* The rails console is printing a Route error in home page:
```
ActionController::RoutingError (No route matches [GET] "/searchid/jqcloud.css")
```


# Review - 14/12/2016 - Phase 3

* Reviewed by Arthur Del Esposte
* Version: b04fa8455c57d4817f69bb98439c2e902190a186

## Verifications

* [ ] Test passing
  * **Comment:** Rspec does not run. Most cucumber test are breaking
* [ ] High test coverage
  * **Comment:** Could not measure test coverage, but there are missing many tests to be written
* [x] Table sort
* [~] Add field "subject"
  * You missed to put this field on the project page
  * The subject was not considered in Cluster algorithm
* [x] Balanced clusters
* [x] Project details
* [x] User Interface
* [x] Configuration of the number of clusters

## Comments

* Clustering is taking too long
* The system does not work when there is no project in database
* Few and broken tests =(
* It is not a good idea add Javascript and CSS code into HTML views
* Do not comment an undesired code, remove it!


# Review - 23/11/2016 - Phase 2

* Reviewed by Arthur Del Esposte
* Version: 20e46c8f3ed568950ecaa264f7b615b53ab4712d

## Verifications

* [x] Setup explanation in README
* [ ] Test passing
  * **Comment:** Rspec does not run and Cucumber is breaking
* [ ] High test coverage
  * **Comment:** Could not measure test coverage, but there are missing many tests to be written
* [x] General index table
* [x] Table with clusters
* [x] Project details
* [x] Navigate by cluster

## Suggestions

### Bugs

* The link generated when you follow a cluster does not load the same resource.
This is not compatible with REST ideas. If you reload the page twice, you'll
get different cluster navigations. For example:

`http://localhost:3000/navigator/navigator?action=navigator&controller=navigator&view_projects=0`

* The cluster page always render four blocks to render different tag clusters,
even when there is less than four cluster. Besides, the blank space 
has a link to navigate to the inexisting cluster. If you follow this link, the
system breaks.

### Refactoring

* [TagCloudController:8](app/controllers/tag_cloud_controller.rb) -
Use better names than 'c'
* [NavigatorController](app/controllers/navigator_controller.rb)
  * You should not use global variables. It is not Thread safe
  * The navigator action should be break into smaller private methods
* [Model Project](app/models/project.rb)
  * remove unnecessary relations
  * validation messages are in portuguese
* [Model Project validations](app/models/project.rb) - 
translate validations' messages to english.
* [Application Layout:4](app/views/application.html.erb) - The project's title should be used instead
* [Tag Cloud view:4](app/views/tag_cloud/index.html.haml) - 
The javascript should be placed in a separeted .js file
* [Tag Cloud view:19](app/views/tag_cloud/index.html.haml) - 
Translate text in portuguese
* [Spec tests](spec/models/project_spec) - 
Use better name to describe spec tests. You should not use portuguese messages.


# Review - 14/10/2016 - Phase 1

* Reviewed by Arthur Del Esposte
* Version: 9e5949cf

## Verifications

* [ ] Setup explanation in README
  * **Comment:** Nothing in README
* [ ] Test passing
  * **Comment:** Does not even run
* [ ] High test coverage
  * **Comment:** Could not measure test coverage, but there are missing many tests to be written
* [x] Import data from CSV file
* [x] Create tag cloud
* [x] Clusterize projects by abstract
* [x] Integrate imported data, clusterizer and tag cloud

## Suggestions

### Bugs

* When there is no project in database, the TagCloud page breaks with the following message:

`TypeError (array.dim(=0) < CLASS_DIMENSION(=2)):`

### Refactoring

* [CSV Importer](lib/data_import/csv_importer.rb) - 
It is necessary to refactor csv_importer code into smaller methods
* [TagCloudController:8](app/controllers/tag_cloud_controller.rb) -
Use better names than 'c'
* [Model Project](app/models/project.rb) - remove unnecessary relations
* [Model Project validations](app/models/project.rb) - 
translate validations' messages to english.
* [Application Layout:4](app/views/application.html.erb) - The project's title should be used instead
* [Tag Cloud view:4](app/views/tag_cloud/index.html.haml) - 
The javascript should be placed in a separeted .js file
* [Tag Cloud view:19](app/views/tag_cloud/index.html.haml) - 
Translate text in portuguese
* [Bag Cluster script:1](lib/cluster/bag.rb) - 
Remove unnecessary blank space in first line
* [Bag Cluster script:114](lib/cluster/bag.rb) - 
Remove commented code
* [Headlines Cluster script:1](lib/cluster/headlinges.rb) - 
Remove unnecessary blank space in first line

* [Headlines Cluster script:5](lib/cluster/headlinges.rb) - 
Remove commented code in lines 5 and 6
* [Headlines Cluster script:12](lib/cluster/headlinges.rb) - 
Refactoring clusters method into smaller methods. It's also possible to spread its responsability among other classes
* [Headlines Cluster script:13](lib/cluster/headlinges.rb) - 
There are many variables, such as `k` and `runs` that seem to be configuration paramaters to run the algorithm. Consider to move these kind of variables to a configuration file so that whenever you want to change them you don't need to change the code
* [Headlines Cluster script](lib/cluster/headlinges.rb) - 
It would be more interisting if you create model classes to save clusters data into database
* [Spec tests](spec/models/project_spec) - 
Use better name to describe spec tests. You should not use portuguese messages.


### Code convetions

* Use tabs with two spaces
* Rename camelCase named files to sneak_case style (ex: lib/cluster/ignoredWords.txt)
* Choose a commit message standard. I suggest you to commit in english and add more 
detailed messages
