# Minera Pipe

## Setup

* Install Ruby 2.3.0: You may use [RVM](https://rvm.io/rvm/install)
* Install bundler: `gem install bundler`
* Install dependencies: `bundle install` 
* Create the database: `rake db:create db:migrate`
* Populate database with PIPE projects: `rails runner lib/data_import/csv_importer.rb`
* Populate database with project clusters: `rails runner lib/data_import/cluster_importer.rb`

## Tests

* Rspec tests: `rspec`
* Cucumber tests: `cucumber`
